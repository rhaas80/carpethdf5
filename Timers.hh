#ifndef TIMERS_HH
#define TIMERS_HH

#include <map>
#include <string>
#include <iostream>
#include <sys/time.h>

#define CAT(a,b) x ## b
#define XSTRINGIFY(n) #n
#define STRINGIFY(n) XSTRINGIFY(n)
#define POSITION (__FILE__ " in line " STRINGIFY(__LINE__))

class TimerTag {
  public:
    TimerTag(const std::string& tag) : accTime(&getAccTimes()[tag]) { };
    TimerTag(const TimerTag& tag) : accTime(tag.accTime) { };

    void addToAccTime(double dt) const { *accTime += dt; }
  private:
    double * const accTime;

    std::map<std::string,double>& getAccTimes();
};

inline std::map<std::string,double>& TimerTag::getAccTimes() {
  static struct AccTimesHolder {
    std::map<std::string,double> accTimes;
    ~AccTimesHolder() {
      for(std::map<std::string,double>::const_iterator it = accTimes.begin() ,
                                                       end = accTimes.end() ;
          it != end ;
          ++it) {
        std::cout << it->first << ": " << it->second << " s\n";
      }
    }
  } accTimes;
  return accTimes.accTimes;
}

class Timer {
  public:
    Timer(const TimerTag& tag_);
    ~Timer();

    void restart();
    void stop();

  private:
    bool running;
    double startTime;
    const TimerTag tag;

    double getTime() const;

    // forbid copying and assignment
    Timer(const Timer&);
    Timer& operator=(const Timer&);
};

inline Timer::Timer(const TimerTag& tag_) : tag(tag_) {
  restart();
}

inline Timer::~Timer() {
  stop();
}

inline void Timer::restart() {
  startTime = getTime();
  running = true;
}

inline void Timer::stop() {
  if(running)
    tag.addToAccTime(getTime() - startTime);
  running = false;
}

inline double Timer::getTime() const {
  timeval tv;
  const int ierr = gettimeofday(&tv, NULL);
  if(!ierr) {
    return tv.tv_sec + tv.tv_usec * 1e-6;
  } else {
    return 0.;
  }
}  
#endif // TIMERS_HH
